import java.util.Date;

//учет транспортных средств

public class Accounting {
    private int transportId;
    private Date startRent;
    private Date endRent;
    private int startedParkingAreaId;
    private int endedParkingAreaId;
    private String status;

    public Accounting(int transportId,Date startRent, Date endRent, int startedParkingAreaId, int endedParkingAreaId,String status){
        this.transportId=transportId;
        this.startRent=startRent;
        this.endRent=endRent;
        this.startedParkingAreaId=startedParkingAreaId;
        this.endedParkingAreaId=endedParkingAreaId;
        this.status=status;
    }

}
