public class Car extends Transport {

    private int patrolAmount;
    public Car(int id, String typeOfTransport, String condition,int parkingAreaId,int patrolAmount){
        super(id,typeOfTransport,condition,parkingAreaId);
        this.patrolAmount=patrolAmount;
    }
}
