public class ElectricScooter extends Transport {

    private int batteryCharge;
    private int maxSpeed;


    public ElectricScooter(int id, String typeOfTransport, String condition,int parkingAreaId,int batteryCharge, int maxSpeed){
        super(id,typeOfTransport,condition,parkingAreaId);
        this.batteryCharge=batteryCharge;
        this.maxSpeed=maxSpeed;
    }


}
