public class ParkingArea {
    private int id;
    private double coordinatesX;
    private double coordinatesY;
    private int parkingRadius;
    private String type;

    public ParkingArea(int id, double coordinatesX, double coordinatesY, int parkingRadius, String type){
        this.id=id;
        this.coordinatesX=coordinatesX;
        this.coordinatesY=coordinatesY;
        this.parkingRadius=parkingRadius;
        this.type=type;
    }

}
