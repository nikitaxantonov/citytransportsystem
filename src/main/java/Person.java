import java.util.HashMap;
import java.util.Map;

public class Person {
    private String name;
    private String email;
    private String password;

    private boolean reading = false;
    private boolean changing = false;
    private boolean deleting = false;

    public Person(String name, String email, String password,boolean statement){
        this.name=name;
        this.email=email;
        this.password=password;
        this.reading=statement;
        this.changing=statement;
        this.deleting=statement;
    }

    public String getEmail(){
        return email;
    }

    public String getPassword(){
        return password;
    }

    public String getName(){ return name; }
}
