import java.util.ArrayList;

public class Transport {
    private int id;
    private String typeOfTransport;
    private String condition;
    private int parkingAreaId;

    private ArrayList<Transport> transportArrayList = new ArrayList<>();

    public void addTransport(Transport transport){
        switch(transport.typeOfTransport){
            case "Car":
                transportArrayList.add(new Car(transport.id,transport.typeOfTransport,transport.condition,transport.parkingAreaId,100));
                break;
            case "Electric scooter":
                transportArrayList.add(new ElectricScooter(transport.id, transport.typeOfTransport, transport.condition,transport.parkingAreaId,100, 75));
                break;
        }
    }

    public Transport getTransportByType(String type){
        for(Transport s: transportArrayList){
            if(s.getTypeOfTransport().equals(type)){
                return s;
            }
        }
        return null;
    }

    public String getTypeOfTransport(){
        return typeOfTransport;
    }

    public Transport(int id, String typeOfTransport, String condition,int parkingAreaId){
        this.id=id;
        this.typeOfTransport=typeOfTransport;
        this.condition=condition;
        this.parkingAreaId=parkingAreaId;
    }


}
