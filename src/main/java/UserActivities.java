import exceptions.WrongLoginException;

import java.util.ArrayList;
import java.util.Scanner;

public class UserActivities {

    private static ArrayList<Person> personArrayList = new ArrayList<Person>();

    public boolean isLogin(String email, String password) throws WrongLoginException {
        boolean loginFlag = false;
        try {
            for(Person i:personArrayList){
            if(i.getEmail().equals(email) && i.getPassword().equals(password)){
                loginFlag=true;
                break;
            }
        }
        }catch(Exception e){
            throw new WrongLoginException("Wrong login or password");
        }
        return loginFlag;
    }

    public static void register(Person person){
        personArrayList.add(person);
    }

    public void login(Person person) throws WrongLoginException{
        if(!isLogin(person.getEmail(),person.getPassword())){
            System.out.println("Invalid login or password, do you want to begin registration?(Y/N)");
            Scanner scanner = new Scanner(System.in);
            if(scanner.nextLine().equals("Y")){
                register(person);
            }
            if(scanner.nextLine().equals("N")){
                System.out.println("Try again or press exit");
            }
        }else{
            System.out.println("Welcome back, " + person.getName());
        }
    }
}
